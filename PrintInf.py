class PrintInf:
    def __init__(self):
        self.enabled = True

    def __call__(self, f):
        def wrap(*args, **kwargs):
            if self.enabled:
                print('Calling {}'.format(f))
            return f(*args, **kwargs)
        return wrap

print_inf = PrintInf()

@print_inf
def rotate_list(l):
    return l[1:] + [l[0]]
